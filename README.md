ZAZ Cfdi

Extensión para LibreOffice con varias utilidades para manejar los CFDI del SAT


Hecha con: https://gitlab.com/mauriciobaeza/zaz


### Software libre, no gratis


Esta extensión tiene un costo de 200 MXN anuales


BCH: `qztd3l00xle5tffdqvh2snvadkuau2ml0uqm4n875d`

BTC: `3FhiXcXmAesmQzrNEngjHFnvaJRhU1AGWV`


<BR>
<BR>
<BR>
Se requiere tener instalado:

* httpx
* lxml

si usas Windows ademas:

* curio
