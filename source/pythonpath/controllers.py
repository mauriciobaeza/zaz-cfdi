#!/usr/bin/env python3

import asyncio
import re
import sys
import threading

import httpx
import lxml.etree as ET

import qrcode
import qrcode.image.svg as svg

import easymacro as app


if app.IS_WIN:
    import curio


DEBUG = True

TIMEOUT = 30

APP = 'zaz-cfdi'

NS_CFDI = {
    'cfdi': 'http://www.sat.gob.mx/cfd/3',
    'tfd': 'http://www.sat.gob.mx/TimbreFiscalDigital',
}

CADENA = '||{Version}|{UUID}|{FechaTimbrado}|{SelloCFD}|{NoCertificadoSAT}||'

TAXES = {
    '000': 'EXENTO',
    '001': 'ISR {0:.2f}%',
    '002': 'IVA {0:.2f}%',
    '003': 'IEPS {0:.2f}%',
}

HEADERS = ('ClaveProdServ', 'NoIdentificacion', 'Cantidad', 'ClaveUnidad',
    'Unidad', 'Descripcion', 'ValorUnitario', 'Importe')
HEADERS32 = ('ClaveProdServ', 'noIdentificacion', 'cantidad', 'ClaveUnidad',
    'unidad', 'descripcion', 'valorUnitario', 'importe')

xml_error = []


def get_qr(data):
    info = ('https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?'
        f"&id={data['timbre']['UUID']}"
        f"&re={data['emisor']['Rfc']}"
        f"&rr={data['receptor']['Rfc']}"
        f"&tt={data['cfdi']['Total']}"
        f"&fe={data['cfdi']['Sello'][-8:]}"
    )
    factory = svg.SvgImage
    qr = qrcode.make(info, border=2, image_factory=factory)

    path = app.get_temp_file(True)
    qr.save(path)

    return path


def xml_to_dict(path, for_report=False):
    data  = {}

    recovering_parser = ET.XMLParser(recover=True)
    doc = ET.parse(path, parser=recovering_parser)
    root = doc.getroot()
    data['cfdi'] = dict(root.attrib)
    data['cfdi'].pop('Certificado', '')

    if 'version' in data['cfdi']:
        data['cfdi']['Version'] = data['cfdi']['version']
    version = data['cfdi']['Version']

    for node in root:
        tag = ET.QName(node).localname.lower()
        if tag in ('emisor', 'receptor'):
            data[tag] = dict(node.attrib)
        else:
            data[tag] = node

    def _get_tax(attrib):
        tax = dict(attrib)
        if version == '3.2':
            if 'tasa' in tax:
                tasa = f"{float(tax['tasa']):.2f}"
            else:
                tasa = 's/t'
            tax['name'] = f"{tax['impuesto']} {tasa}"
            return tax

        if 'TasaOCuota' in tax:
            tasa = float(tax['TasaOCuota']) * 100
            tax['name'] = TAXES.get(tax['Impuesto']).format(tasa)
        else:
            name, _ = TAXES.get(tax['Impuesto']).split(' ')
            tax['name'] = name
            if 'TipoFactor' in tax:
                tax['name'] = f"{name} {tax['TipoFactor']}"

        if 'Importe' in tax:
            tax['Importe'] = float(tax['Importe'])

        return tax

    if 'impuestos' in data:
        for node in data['impuestos']:
            tag = ET.QName(node).localname.lower()
            data[tag] = [_get_tax(n.attrib) for n in node]
        data['impuestos'] = dict(data['impuestos'].attrib)

    conceptos = []
    for node in data['conceptos']:
        concepto = dict(node.attrib)
        for key in ('Cantidad', 'ValorUnitario', 'Importe'):
            concepto[key] = float(concepto[key])
        for taxes in node:
            t = ET.QName(taxes).localname.lower()
            if t != 'impuestos':
                # ToDo ComplementoConcepto
                continue
            for tax in taxes:
                tag = ET.QName(tax).localname.lower()
                concepto[tag] = [_get_tax(t.attrib) for t in tax]
        conceptos.append(concepto)
    data['conceptos'] = conceptos

    timbre = root.xpath('//cfdi:Complemento/tfd:TimbreFiscalDigital', namespaces=NS_CFDI)[0]
    data['timbre'] = dict(timbre.attrib)

    if not for_report:
        data['path_qr'] = get_qr(data)

    if version == '3.2':
        data['cfdi']['SubTotal'] = float(data['cfdi']['subTotal'])
        data['cfdi']['Total'] = float(data['cfdi']['total'])
    else:
        data['cfdi']['SubTotal'] = float(data['cfdi']['SubTotal'])
        data['cfdi']['Total'] = float(data['cfdi']['Total'])
        if 'Descuento' in data['cfdi']:
            data['cfdi']['Descuento'] = float(data['cfdi']['Descuento'])
    if not for_report:
        data['cfdi']['cadenaoriginal'] = CADENA.format(**data['timbre'])
        letters = NumberToLetters(data['cfdi']['Total'], data['cfdi']['Moneda']).letters
        data['cfdi']['totalenletras'] = letters

    return data


def _get_template(path_source, data):
    emisor = f"{data['emisor']['Rfc'].lower()}.ods"
    path = app.join(path_source, emisor)
    if app.exists_path(path):
        return path

    receptor = f"{data['receptor']['Rfc'].lower()}.ods"
    path = app.join(path_source, receptor)
    if app.exists_path(path):
        return path

    path = app.join(path_source, f'receptor_{receptor}')
    if app.exists_path(path):
        return path

    return ''


def _set_totales(sheet, data):
    totales = data.get('traslados', [])
    if 'Descuento' in data['cfdi']:
        descuento = {'name': 'Descuento', 'Importe': data['cfdi']['Descuento']}
        totales.insert(0, descuento)

    first_title = sheet['total']
    first_value = sheet['importe']
    for total in totales:
        next_cell1 = first_title[0,0].offset(1)
        next_cell2 = first_value[0,0].offset(1)
        next_cell1.copy(first_title)
        next_cell2.copy(first_value)
        first_title.value = total['name']
        first_value.value = total['Importe']
        first_title = next_cell1
        first_value = next_cell2
    return


@app.run_in_thread
def export_documents(path_source, files):
    t = len(files)
    doc = app.get_document()
    sb = doc.statusbar
    sb.start(f'Exportando {t} documentos', t)
    c = 0
    for i, f in enumerate(files):
        sb.setValue(i + 1)
        if _export_to_pdf(path_source, f):
            c += 1

    sb.end()
    if c:
        msg = f'{t} documentos XML encontrados\n{c} documentos PDF generados'
        app.msgbox(msg)
    else:
        app.errorbox('No se generaron documentos')
    return


def _export_to_pdf(path_source, path_xml):
    if DEBUG:
        msg = f'XML: {path_xml}'
        app.debug(msg)

    path, _, name, _ = app.get_info_path(path_xml)
    path_pdf = app.join(path, f'{name}.pdf')
    data = xml_to_dict(path_xml)

    path_template = _get_template(path_source, data)
    if not path_template:
        msg = 'No se encontró la plantilla ODS'
        app.error(msg)
        return False

    args = {'AsTemplate': True, 'Hidden': True}
    doc = app.open_doc(path_template, **args)
    path_qr = data.pop('path_qr')
    sheet = doc[0]
    cell = sheet.find('timbre.cbb')
    rango = sheet.render(data, clean=True)
    args = {'Width': 4300, 'Height': 4500}
    cell.insert_image(path_qr, **args)

    _set_totales(sheet, data)

    doc.to_pdf(path_pdf)
    doc.close()
    app.kill(path_qr)

    return True


class MakePdf(object):

    def __init__(self, dialog):
        self.d = dialog

    def cmd_source_action(self, event):
        self.d.txt_source.value = app.get_dir()
        return

    def cmd_pdf_action(self, event):
        path_source = self.d.txt_source.value
        if not path_source:
            msg = 'No se ha seleccionado una ruta origen'
            app.errorbox(msg)
            return

        if not app.exists_path(path_source):
            msg = 'La ruta origen no existe'
            app.errorbox(msg)
            return

        files = app.get_path_content(path_source, 'xml')
        t = len(files)

        msg = f'Se van a generar {t} documentos\n\n¿Estás seguro de continuar?'
        if not app.question(msg, 'ZAZ CFDI'):
            return

        values = path_source
        app.set_config('pdf', values, APP)
        export_documents(path_source, files)
        return


async def _post(client, data):
    url = 'https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc'
    try:
        r = await client.post(url, data=data, timeout=TIMEOUT)
        assert r.status_code == httpx.codes.OK
        status = re.search("(?s)(?<=Estado>).+?(?=</a:)", r.text).group()
    except Exception as e:
        status = str(e)
    return status


# ~ async def _post_win(client, data):
def _post_win(client, data):
    url = 'https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc'
    try:
        r = client.post(url, data=data, timeout=TIMEOUT)
        if r.status_code == httpx.codes.OK:
            status = re.search("(?s)(?<=Estado>).+?(?=</a:)", r.text).group()
    except Exception as e:
        status = str(e)
    return status


async def _get_status_sat(rows):
    soap = """<?xml version="1.0" encoding="UTF-8"?>
    <soap:Envelope
        xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <soap:Header/>
        <soap:Body>
        <Consulta xmlns="http://tempuri.org/">
            <expresionImpresa>
                ?re={emisor_rfc}&amp;rr={receptor_rfc}&amp;tt={total}&amp;id={uuid}
            </expresionImpresa>
        </Consulta>
        </soap:Body>
    </soap:Envelope>"""
    headers = {
        'SOAPAction': '"http://tempuri.org/IConsultaCFDIService/Consulta"',
        'Content-type': 'text/xml; charset="UTF-8"'
    }

    tasks = []
    async with httpx.AsyncClient(headers=headers) as client:
        for row in rows:
            data = soap.format(**row).encode('utf-8')
            task = asyncio.create_task(_post(client, data))
            tasks.append(task)

    return await asyncio.gather(*tasks)


async def _get_status_sat_win(rows):
    soap = """<?xml version="1.0" encoding="UTF-8"?>
    <soap:Envelope
        xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <soap:Header/>
        <soap:Body>
        <Consulta xmlns="http://tempuri.org/">
            <expresionImpresa>
                ?re={emisor_rfc}&amp;rr={receptor_rfc}&amp;tt={total}&amp;id={uuid}
            </expresionImpresa>
        </Consulta>
        </soap:Body>
    </soap:Envelope>"""
    headers = {
        'SOAPAction': '"http://tempuri.org/IConsultaCFDIService/Consulta"',
        'Content-type': 'text/xml; charset="UTF-8"'
    }

    tasks = []
    async with curio.TaskGroup() as tg:
        with httpx.Client(headers=headers) as client:
            for row in rows:
                data = soap.format(**row).encode('utf-8')
                await tg.spawn(curio.run_in_thread(_post_win, client, data))
            tasks = [t.result async for t in tg]

    return tasks


def get_status_sat(info_sat):
    if app.IS_WIN:
        status_sat = curio.run(_get_status_sat_win(info_sat))
    else:
        status_sat = asyncio.run(_get_status_sat(info_sat))
    return status_sat


def _get_row(path_xml, fields):
    try:
        data = xml_to_dict(path_xml, for_report=True)
    except Exception as e:
        print(path_xml)
        xml_error.append(path_xml)
        print(e)
        return (), ()

    version = data['cfdi']['Version']

    def _prepare_data(data):
        new_data = {}
        for k1, v1 in data.items():
            key = k1.lower()
            if isinstance(v1, dict):
                for k2, v2 in v1.items():
                    if k2.lower() in ('sello', 'sellocfd', 'sellosat'):
                        continue
                    key = f'{k1.lower()}.{k2.lower()}'
                    new_data[key] = v2
            else:
                new_data[key] = v1

        new_data['cfdi.fecha'] = new_data['cfdi.fecha'].replace('T', ' ')
        new_data['timbre.fechatimbrado'] = new_data['timbre.fechatimbrado'].replace('T', ' ')
        return new_data


    details = False
    if 'detalle' in fields:
        details = True
        fields.remove('detalle')

    conceptos = data.pop('conceptos')
    traslados = data.pop('traslados', [])
    complemento = data.pop('complemento')
    data = _prepare_data(data)

    for traslado in traslados:
        name, tasa = traslado['name'].split(' ')
        key = f"traslado.{name.lower()}.{tasa[:-1]}"
        tax_name = 'Importe'
        if version == '3.2':
            tax_name = 'importe'
        data[key] = traslado[tax_name]

    if 'conceptos' in fields:
        rows = []
        if version == '3.2':
            for row in conceptos:
                key = row.get('noIdentificacion', 'sin')
                price = f"${float(row['valorUnitario']):,.2f}"
                line = f"Clave: {key}, {row['descripcion']} [{price}]"
                rows.append(line)
        else:
            for row in conceptos:
                key = row.get('NoIdentificacion', 'sin')
                price = f"${float(row['ValorUnitario']):,.2f}"
                line = f"Clave: {key}, {row['Descripcion']} [{price}]"
                rows.append(line)
        data['conceptos'] = ' | '.join(rows)

    if 'estatus_sat' in fields:
        info_sat = {
            'emisor_rfc': data['emisor.rfc'],
            'receptor_rfc': data['receptor.rfc'],
            'total': str(data['cfdi.total']),
            'uuid': data['timbre.uuid']
        }
        data['estatus_sat'] = info_sat

    row = [data.get(f, f) for f in fields]

    rc = []
    if details:
        sin = 'sin'
        headers = HEADERS
        if version == '3.2':
            headers = HEADERS32
        for c in conceptos:
            concepto = [c.get(f, sin) for f in headers]
            concepto[2] = float(concepto[2])
            concepto[6] = float(concepto[6])
            concepto[7] = float(concepto[7])
            rc.append(concepto)

    return row, rc


class Report(object):

    def __init__(self, dialog):
        self.d = dialog

    def cmd_source_dir_action(self, event):
        self.d.txt_source_dir.value = app.get_dir()
        return

    def cmd_source_template_action(self, event):
        self.d.txt_source_template.value = app.get_file(filters=(('ODS', '*.ods'),))
        return

    def cmd_report_action(self, event):
        path_source = self.d.txt_source_dir.value
        path_template = self.d.txt_source_template.value
        if not path_source:
            msg = 'No se ha seleccionado una ruta origen'
            app.errorbox(msg)
            return

        if not app.exists_path(path_source):
            msg = 'La ruta origen no existe'
            app.errorbox(msg)
            return

        if not path_template:
            msg = 'No se ha seleccionado la plantilla para el reporte'
            app.errorbox(msg)
            return

        if not app.exists_path(path_source):
            msg = 'La plantilla no existe'
            app.errorbox(msg)
            return

        values = (path_source, path_template)
        app.set_config('report', values, APP)

        c = 0
        files = app.get_path_content(path_source, 'xml')
        t = len(files)

        try:
            msg = f'Se van a reportar {t} documentos\n\n¿Estás seguro de continuar?'
            if not app.question(msg, 'ZAZ CFDI'):
                return

            args = {'AsTemplate': True, 'Hidden': True}
            template = app.open_doc(path_template, **args)
            cell = template[0]['A1']
            fields = cell.current_region.data[0]
            fields = [f.lower() for f in fields]

            rows = []
            for f in files:
                data, conceptos = _get_row(f, fields[:])
                if data:
                    if conceptos:
                        for concepto in conceptos:
                            rows.append(data + concepto)
                    else:
                        rows.append(data)

            if 'detalle' in fields:
                headers = (tuple(fields[:-1]) + HEADERS,)
                cell.copy_from(headers)

            if 'estatus_sat' in fields:
                col = fields.index('estatus_sat')
                info_sat = [r[col] for r in rows]
                info_sat = get_status_sat(info_sat)
                for i, r in enumerate(rows):
                    r[col] = info_sat[i]

            cell.next_cell.copy_from(rows)

        except Exception as e:
            app.msgbox(str(e))

        template.visible = True

        return


class NumberToLetters(object):

    def __init__(self, value, moneda, **args):
        self._letras = self._letters(value, moneda)

    @property
    def letters(self):
        return self._letras.upper()

    def _letters(self, numero, currency='MXN'):
        monedas = {
            'MXN': 'peso',
            'USD': 'dólar',
            'EUR': 'euro',
        }
        moneda = monedas.get(currency, currency)
        tf = {
            'MXN': 'm.n.',
        }

        texto_inicial = '-('
        texto_final = '/100 {})-'.format(tf.get(currency, currency))
        fraccion_letras = False
        fraccion = ''

        enletras = texto_inicial
        numero = abs(numero)
        numtmp = '%015d' % numero

        if numero < 1:
            enletras += 'cero ' + self._plural(moneda) + ' '
        else:
            enletras += self._numlet(numero)
            if numero == 1 or numero < 2:
                enletras += moneda + ' '
            elif int(''.join(numtmp[3:])) == 0 or int(''.join(numtmp[9:])) == 0:
                enletras += 'de ' + self._plural(moneda) + ' '
            else:
                enletras += self._plural(moneda) + ' '

        decimal = '%0.2f' % numero
        decimal = decimal.split('.')[1]
        #~ decimal = int((numero-int(numero))*100)
        if fraccion_letras:
            if decimal == 0:
                enletras += 'con cero ' + self._plural(fraccion)
            elif decimal == 1:
                enletras += 'con un ' + fraccion
            else:
                enletras += 'con ' + self._numlet(int(decimal)) + self.plural(fraccion)
        else:
            enletras += decimal

        enletras += texto_final
        return enletras

    def _numlet(self, numero):
        numtmp = '%015d' % numero
        co1=0
        letras = ''
        leyenda = ''
        for co1 in range(0,5):
            inicio = co1*3
            cen = int(numtmp[inicio:inicio+1][0])
            dec = int(numtmp[inicio+1:inicio+2][0])
            uni = int(numtmp[inicio+2:inicio+3][0])
            letra3 = self.centena(uni, dec, cen)
            letra2 = self.decena(uni, dec)
            letra1 = self.unidad(uni, dec)

            if co1 == 0:
                if (cen+dec+uni) == 1:
                    leyenda = 'billon '
                elif (cen+dec+uni) > 1:
                    leyenda = 'billones '
            elif co1 == 1:
                if (cen+dec+uni) >= 1 and int(''.join(numtmp[6:9])) == 0:
                    leyenda = "mil millones "
                elif (cen+dec+uni) >= 1:
                    leyenda = "mil "
            elif co1 == 2:
                if (cen+dec) == 0 and uni == 1:
                    leyenda = 'millon '
                elif cen > 0 or dec > 0 or uni > 1:
                    leyenda = 'millones '
            elif co1 == 3:
                if (cen+dec+uni) >= 1:
                    leyenda = 'mil '
            elif co1 == 4:
                if (cen+dec+uni) >= 1:
                    leyenda = ''

            letras += letra3 + letra2 + letra1 + leyenda
            letra1 = ''
            letra2 = ''
            letra3 = ''
            leyenda = ''
        return letras

    def centena(self, uni, dec, cen):
        letras = ''
        numeros = ["","","doscientos ","trescientos ","cuatrocientos ","quinientos ","seiscientos ","setecientos ","ochocientos ","novecientos "]
        if cen == 1:
            if (dec+uni) == 0:
                letras = 'cien '
            else:
                letras = 'ciento '
        elif cen >= 2 and cen <= 9:
            letras = numeros[cen]
        return letras

    def decena(self, uni, dec):
        letras = ''
        numeros = ["diez ","once ","doce ","trece ","catorce ","quince ","dieci","dieci","dieci","dieci"]
        decenas = ["","","","treinta ","cuarenta ","cincuenta ","sesenta ","setenta ","ochenta ","noventa "]
        if dec == 1:
            letras = numeros[uni]
        elif dec == 2:
            if uni == 0:
                letras = 'veinte '
            elif uni > 0:
                letras = 'veinti'
        elif dec >= 3 and dec <= 9:
            letras = decenas[dec]
        if uni > 0 and dec > 2:
            letras = letras+'y '
        return letras

    def unidad(self, uni, dec):
        letras = ''
        numeros = ["","un ","dos ","tres ","cuatro ","cinco ","seis ","siete ","ocho ","nueve "]
        if dec != 1:
            if uni > 0 and uni <= 5:
                letras = numeros[uni]
        if uni >= 6 and uni <= 9:
            letras = numeros[uni]
        return letras

    def _plural(self, palabra):
        if re.search('[aeiou]$', palabra):
            return re.sub('$', 's', palabra)
        else:
            return palabra + 'es'


@app.catch_exception
def main(args):
    path = args[1]
    data = xml_to_dict(path, True)
    print(data)
    return


if __name__ == '__main__':
    main(sys.argv)
